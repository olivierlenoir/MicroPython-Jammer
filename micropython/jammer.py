# Author: Olivier Lenoir - <olivier.len02@gmail.com>
# Created: 2021-09-17 15:59
# License: MIT, Copyright (c) 2021 Olivier Lenoir
# Language: MicroPython v1.17
# Project: MicroPython-Jammer
# Description: Micropython audio jammer for ESP32


from machine import Pin, PWM, Timer
from random import randint
from utime import sleep_ms


class Jammer(object):

    def __init__(self, buzz_pin, min_freq, max_freq, min_period, max_period):
        """Freq (Hz), Period (ms)"""
        self.buzz_pin = buzz_pin
        self._min_freq = min_freq
        self._max_freq = max_freq
        self._min_period = min_period
        self._max_period = max_period
        self.start()

    def _rnd_freq(self):
        """Random frequency"""
        return randint(self._min_freq, self._max_freq)

    def _rnd_period(self):
        """Random period"""
        return randint(self._min_period, self._max_period)

    def _buzz_freq(self):
        """Buzzer frequency"""
        self.buzz.freq(self._rnd_freq())
        self._init_timer()

    def _init_timer(self):
        """One shot timer"""
        self._tim_period = Timer(-1)  # Init vitual timer
        self._tim_period.init(
            period=self._rnd_period(),
            mode=Timer.ONE_SHOT,
            callback=lambda t: self._buzz_freq()
            )

    def start(self):
        """Init timer"""
        self.buzz = PWM(Pin(self.buzz_pin), duty=512, freq=self._rnd_freq())
        self._init_timer()

    def stop(self):
        """Deinit timer"""
        self._tim_period.deinit()
        self.buzz.deinit()

    def min_freq(self, freq=None):
        """Set or return min_freq"""
        if freq is None:
            return self._min_freq
        else:
            self._min_freq = freq

    def max_freq(self, freq=None):
        """Set or return max_freq"""
        if freq is None:
            return self._max_freq
        else:
            self._max_freq = freq

    def min_period(self, period=None):
        """Set or return min_period"""
        if period is None:
            return self._min_period
        else:
            self._min_period = period

    def max_period(self, period=None):
        """Set or return max_period"""
        if period is None:
            return self._max_period
        else:
            self._max_period = period
