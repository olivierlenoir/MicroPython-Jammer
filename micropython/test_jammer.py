# Author: Olivier Lenoir - <olivier.len02@gmail.com>
# Created: 2021-10-23 11:30:08
# License: MIT, Copyright (c) 2021 Olivier Lenoir
# Language: MicroPython v1.17
# Project: Test MicroPython-Jammer
# Description:
#   Use Pin 2
#   Min fréquency 1 Hz
#   Max frequency 10 Hz
#   Min period 5000 ms
#   Max period 10000 ms


from jammer import Jammer


jam_led = Jammer(2, 1, 10, 5000, 10000)  # Jammer start at init


# To stop
jam_led.stop()

# To start
jam_led.start()

# Get min frequency
jam_led.min_freq()

# Change min frequency
jam_led.min_freq(3)

# Get max frequency
jam_led.max_freq()

# Change max frequency
jam_led.max_freq(8)

# Get min period
jam_led.min_period()

# Change min period
jam_led.min_period(300)

# Get max period
jam_led.max_period()

# Change min period
jam_led.max_period(800)
